'use strict';

const BBPromise = require('bluebird');
const sUtil = require('../lib/util');
const HTTPError = sUtil.HTTPError;
const { body, validationResult } = require('express-validator');

/**
 * The main router object
 */
const router = sUtil.router();
const fs = BBPromise.promisifyAll(require('fs'));

router.get('/', (req, res) => {
	res.redirect('/instruments');
});

router.get('/instruments', (req, res) => {
	return fs.readFileAsync(__dirname + '/../static/instruments.html')
		.then(function (src) {
			// TODO Shows the lists of all instruments as an HTML table.
			res.status(200).type('html').send(src);
		});
});

router.get('/instrument/create', (req, res) => {
	return fs.readFileAsync(__dirname + '/../static/create-instrument.html')
		.then(function (src) {
			// Shows the HTML form to create an instrument
			res.status(200).type('html').send(src);
		});
});

router.get('/instrument/:slug', (req, res) => {
	return fs.readFileAsync(__dirname + '/../static/' + req.params.slug + '.html')
		.then(function (src) {
			// TODO Sanitize :slug
			res.status(200).type('html').send(src);
		})
		.catch(function (error) {
			throw new HTTPError({
				status: 404,
				type: 'not found',
				title: 'Not Found',
				detail: 'Instrument not found'
			});
		});
});

router.post('/instruments',
	[
		[
			body('name').notEmpty().trim(),
			body('slug').notEmpty().trim(),
			body('description').trim(),
			body('creator').notEmpty().trim(),
			body('owner').notEmpty().trim(),
			body('purpose').trim(),
			body('start_date').notEmpty().trim(),
			body('end_date').trim(),
			body('task').notEmpty().trim(),
			body('sample_unit').notEmpty().trim(),
			body('sample_rate').notEmpty().trim(),
			body('contextual_attributes').notEmpty().isArray(),
			body('environments').notEmpty().isArray().trim()
		]
	],
	(req, res) => {
		// TODO Validate the double-submit cookie

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			console.log('errors');
			return res.status(400).json({ errors: errors.array() });
		}
		const createdAt =  new Date().toISOString();
		const updatedAt =  new Date().toISOString();

		// TODO Register instrument

		// TODO Add an entry to the SAL

		// TODO Invalidate the in-memory cache for GET /api/v1/instruments

		res.redirect('/:' + req.body.slug);
	});

module.exports = (appObj) => {

	return {
		path: '/',
		skip_domain: true,
		router
	};

};
