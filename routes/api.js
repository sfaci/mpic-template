'use strict';

const sUtil = require('../lib/util');

/**
 * The main router object
 */
const router = sUtil.router();
const instruments = [{
	id: 123456,
	name: 'hello world!',
	slug: 'hello-world',
	description: 'hello world instrument',
	creator: 'Sam Smith',
	owner: 'Data Products',
	purpose: 'To create an instrument',
	created_at: new Date().toISOString(),
	updated_at: new Date().toISOString(),
	start_date: new Date().toISOString(),
	task: 'yolo',
	sample_unit: 'session',
	sample_rate: {
		default: 0.1,

		// group2 -> 0.01
		aawiki: 0.01,
		abwiki: 0.01,
		acewiki: 0.01
	},
	contextual_attributes: [
		'agent_app_install_id',
		'agent_app_flavor',
		'agent_app_theme',
		'agent_app_version',
		'agent_device_language',
		'agent_release_status',
		'mediawiki_database',
		'page_id',
		'page_title',
		'page_namespace_id',
		'page_wikidata_qid',
		'page_content_language',
		'performer_is_logged_in',
		'performer_session_id',
		'performer_pageview_id',
		'performer_language_groups',
		'performer_language_primary',
		'performer_groups'
	],
	environments: ['production']
}];

router.get('/instruments', (req, res) => {
	res.status(200).json(instruments);
});

module.exports = (appObj) => {

	return {
		path: '/',
		api_version: 1,
		router
	};

};
