# mpic-template

Template for creating MPIC project

## Getting Started

Install the dependencies

```
cd service-template-node
npm install
```

### Running the examples

The template is a fully-working example, so you may try it right away. To
start the server hosting the REST API, simply run (inside the repo's directory)

```
npm start
```

In `routes/mpic.js` file:

* GET `http://localhost:8080`: Redirects to `http://localhost:8080/instruments`
* GET `http://localhost:8080/instruments`: Shows the instrument list
* GET `http://localhost:8080/:slug`: Shows the details about an specific instrument (according to the :slug specified)
* GET `http://localhost:8080/create`: Shows the form to register a new instrument
* POST `http://localhost:8080/instruments`: Registers a new instrument

In `routes/api.js` file:

* GET 'http://localhost:8080/api/v1/instruments': Returns a JSON representation of all registered instruments

### Tests

The template also includes a test suite a small set of executable tests. To fire
them up, simply run:

```
npm test
```

If you haven't changed anything in the code (and you have a working Internet
connection), you should see all the tests passing. As testing most of the code
is an important aspect of service development, there is also a bundled tool
reporting the percentage of code covered. Start it with:

```
npm run-script coverage
```

### Issue Tracking

We use Phabricator to track and create new issues. See the list of [open issues](https://phabricator.wikimedia.org/project/board/1072/query/open/) and feel free to submit a [new one](https://phabricator.wikimedia.org/maniphest/task/edit/form/1/?projects=PHID-PROJ-quztkvlbvay2bszm6w4v).

### Docker

In order to generate the Docker image, the blubber file needs to be configured and [Buidkit](https://github.com/moby/buildkit) must be installed.

To create the Dockerfile (for development purposes):
```
docker build --target development -f .pipeline/blubber.yaml -t mpic .
```

You can change the target name in the above command to build the image according to any existing `variant` in the blubber file (for example: development, test or production)

To run the service as a docker container locally, there is a `docker-compose.yaml` file in the repository already configured to do it. Just run the following command:

```
docker compose up
```

### Troubleshooting

In a lot of cases when there is an issue with node it helps to recreate the
`node_modules` directory:

```
rm -r node_modules
npm install
```
